-module(worker_SUITE).
-compile(export_all).
-include_lib("common_test/include/ct.hrl").

suite() -> [{timetrap, {seconds, 24}}].

all() -> [ start_and_stop, ping, stash ].

init_per_testcase(start_and_stop, C) -> C;
init_per_testcase(_T, C) ->
    {ok, InitState} = hachi_worker:init([]),
    [ { state, InitState } | C ].

end_per_testcase(_T, C) -> C.

start_and_stop(_C) ->
    {ok, Pid} = hachi_worker:start_anon(),
    true = is_process_alive(Pid),
    stopped = gen_server:call(Pid, stop),
    false = is_process_alive(Pid).

ping(C) ->
    State = ?config(state, C),
    {reply, Id, _NewState} = hachi_worker:handle_call(id, nopid, State),
    <<_Hid:32/binary>> = Id.

stash(C) ->
    S0 = ?config(state, C),
    % create some dummy data
    Key1 = crypto:rand_bytes(32),
    Key0 = crypto:rand_bytes(32),
    true = (Key0 /= Key1), % random sanity
    Val = << "testing:", (crypto:rand_bytes(16))/binary >>,
    % and now try store and retrieve
    {noreply, S1} = hachi_worker:handle_cast({store, Key1, Val}, S0),
    {reply, {value, Val}, _S2a} = hachi_worker:handle_call({retrieve, Key1}, nopid, S1),
    {reply, none, _S2b} = hachi_worker:handle_call({retrieve, Key1}, nopid, S0),
    {reply, none, _S2c} = hachi_worker:handle_call({retrieve, Key0}, nopid, S1).

