-module(contacts_SUITE).
-compile(export_all).
-include_lib("common_test/include/ct.hrl").

suite() -> [{timetrap, {seconds, 20}}].

all() -> [ {group, G} || G <- proplists:get_keys(groups()) ].

groups() ->
    Tests = [ params, lookup_nothing, add_a_node ],
    [
        { byte0, [], Tests },
        { byte128, [], Tests },
        { small, [], Tests },
        { standard, [], Tests }
    ].

% Tests for contacts
%
% 1. lookup id on empty contact list; get nothing
% 1. add a node, get it back
% 2. remove a node, get nothing back
% 3. add a node to full big bucket, causing a split
% 4. add a node to full bucket; fail, return lru node
% 5. (multiple combos) add some nodes, the get closest N nodes to arg
%
% Also need tests for contact list manager server.

% get id from hachi:new_id();
% test tree splitting
%   set my id == 0
%   add k small ids
%   should get 255-ish buckets
%   ask for bucket_stats
% test tree splitting for unbalanced tree
% add a large number of ids to the table
% ask for nearest to


% init_per_suite(C) -> [ {me, hachi:new_id()}, {k,20} | C ].
init_per_group(byte0, C) -> [ {me, <<0>>}, {k,3} | C ];
init_per_group(byte128, C) -> [ {me, <<128>>}, {k,3} | C ];
init_per_group(small, C) -> [ {me, <<0>>}, {k,3} | C ];
init_per_group(_G, C) -> [ {me, hachi:new_id()}, {k,20} | C ].
end_per_group(_G, _C) -> ok.

init_per_testcase(_T, C) -> [ {contacts, hachi_contacts:new(?config(me, C), ?config(k, C))} | C ].

% test params for my id, which affects bit size, and k
params(C) -> 
    Contacts = ?config(contacts, C),
    Id = ?config(me, C),
    K = ?config(k, C),
    Id = hachi_contacts:me(Contacts),
    Bytes = byte_size(Id),
    Bytes = hachi_contacts:key_byte_size(Contacts),
    K = hachi_contacts:k(Contacts).

% lookup a random id should return nothing
lookup_nothing(C) ->
    Contacts = ?config(contacts, C),
    OtherId = hachi:new_id(hachi_contacts:key_byte_size(Contacts)),
    [] = hachi_contacts:lookup(OtherId, Contacts).

% add a node, then lookup to check
add_a_node(C) ->
    Contacts = ?config(contacts, C),
    OtherId = hachi:new_id(hachi_contacts:key_byte_size(Contacts)),
    NewContacts = hachi_contacts:update(OtherId, nopid, Contacts),
    [{OtherId, nopid, _Distance}] = hachi_contacts:lookup(OtherId, NewContacts).

