-module(bucket).
-export([new/1, store/3]).

-record(hachi_contacts, {me, k, buckets}).
-record(bucket, {min, max, nodes}).
-record(tree, {mid, left, right}).

new(MyId) -> #hachi_contacts{ me=MyId, k=3,buckets=#bucket{min=0, max=256, nodes=[]} }.

store(Key, Value, #hachi_contacts{ me=Base, k=K, buckets=Buckets }=Self) ->
    case Key of
        Base -> Self;
        _ ->
            Distance = Key bxor Base,
            Self#hachi_contacts{ buckets=store(Distance, K, Key, Value, Buckets) }
    end.

split(#bucket{min=Min, max=Max, nodes=Nodes}) ->
    Mid = Max bsr 1,
    {Left,Right} = lists:partition(fun({_,_,D}) -> D < Mid end, Nodes),
    #tree{ mid=Mid,
       left=#bucket{min=Min, max=Mid, nodes=Left},
       right=#bucket{min=Mid, max=Max, nodes=Right}
      }.

store(Distance, K, Key, Value, #tree{mid=M, left=L, right=R}=T) ->
    if
        Distance < M -> T#tree{left=store(Distance, K, Key, Value, L)};
        true -> T#tree{right=store(Distance, K, Key, Value, R)}
    end;
store(Distance, K, Key, Value, #bucket{min=Min, max=Max, nodes=Nodes}=B) ->
    if
        length(Nodes) < K -> B#bucket{nodes=[{Key, Value, Distance} | Nodes]};
        Min == 0, Max > 1 ->
            T = split(B),
            store(Distance, K, Key, Value, T);
        true -> B % TODO should return a full message to trigger dead node purging
    end.

