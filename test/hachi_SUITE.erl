-module(hachi_SUITE).
-compile(export_all).
-include_lib("common_test/include/ct.hrl").

suite() -> [{timetrap, {seconds, 24}}].

all() -> [ gen_id, gen_short_id, distance ].

gen_id(_C) ->
    <<Id1:256/big-integer>> = hachi:new_id(),
    <<Id2:256/big-integer>> = hachi:new_id(),
    true = (Id1 /= Id2).

gen_short_id(_C) ->
    <<Id1:16/big-integer>> = hachi:new_id(2),
    <<Id2:16/big-integer>> = hachi:new_id(2),
    true = (Id1 /= Id2).

distance(_C) ->
    0 = hachi:distance(<<1:256/big-integer>>, <<1:256/big-integer>>),
    1 = hachi:distance(<<0:256/big-integer>>, <<1:256/big-integer>>),
    3 = hachi:distance(<<1:256/big-integer>>, <<2:256/big-integer>>),
    X = hachi:new_id(),
    Y = hachi:new_id(),
    0 = hachi:distance(X,X),
    0 = hachi:distance(Y,Y),
    D = hachi:distance(X,Y),
    D = hachi:distance(Y,X),
    true = (D /= 0).
