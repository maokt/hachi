-module(hachi).
-behaviour(application).
-export([start/2, stop/1]).
-export([generate_id/0]).

start(_StartType, _StartArgs) -> hachi_super:start_link().
stop(_State) -> ok.

generate_id() -> crypto:rand_bytes(32).

