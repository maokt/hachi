-module(hachi_contacts).
-export([new/1, new/2, lookup/2, update/3, remove/2]).
-export([me/1, key_byte_size/1, k/1]).

-record(hachi_contacts, {me, k, buckets}).
-record(bucket, {min, max, nodes}).
-record(tree, {mid, left, right}).

% TODO MyId is a binary, but internally we need an integer
new(MyId, K) -> #hachi_contacts{ me=MyId, k=K, buckets=#bucket{min=0, max=256, nodes=[]} }.
new(MyId) -> new(MyId, 20).

me(#hachi_contacts{me=Id}) -> Id.
key_byte_size(#hachi_contacts{me=Id}) -> byte_size(Id).
k(#hachi_contacts{k=K}) -> K.

% TODO
remove(_Key, _Contacts) -> ok.

lookup(Key, #hachi_contacts{ me=MyId, buckets=Buckets }) ->
    case Key of
        MyId -> [MyId];
        _ ->
            Distance = hachi:distance(Key, MyId),
            lookup(Distance, Key, Buckets)
    end.

lookup(Distance, Key, #tree{mid=M, left=L, right=R}) ->
    if
        Distance < M -> lookup(Distance, Key, L);
        true -> lookup(Distance, Key, R)
    end;
lookup(_Distance, _Key, #bucket{nodes=Nodes}) -> Nodes.

update(Key, Value, #hachi_contacts{ me=MyId, k=K, buckets=Buckets }=Self) ->
    case Key of
        MyId -> Self;
        _ ->
            Distance = hachi:distance(Key, MyId),
            Self#hachi_contacts{ buckets=store(Distance, K, Key, Value, Buckets) }
    end.


split(#bucket{min=Min, max=Max, nodes=Nodes}) ->
    Mid = Max bsr 1,
    {Left,Right} = lists:partition(fun({_,_,D}) -> D < Mid end, Nodes),
    #tree{ mid=Mid,
       left=#bucket{min=Min, max=Mid, nodes=Left},
       right=#bucket{min=Mid, max=Max, nodes=Right}
      }.

store(Distance, K, Key, Value, #tree{mid=M, left=L, right=R}=T) ->
    if
        Distance < M -> T#tree{left=store(Distance, K, Key, Value, L)};
        true -> T#tree{right=store(Distance, K, Key, Value, R)}
    end;
store(Distance, K, Key, Value, #bucket{min=Min, max=Max, nodes=Nodes}=B) ->
    if
        length(Nodes) < K -> B#bucket{nodes=[{Key, Value, Distance} | Nodes]};
        Min == 0, Max > 1 ->
            T = split(B),
            store(Distance, K, Key, Value, T);
        true -> B % TODO should return a full message to trigger dead node purging
    end.

