-module(hachi_worker).
-behaviour(gen_server).
-export([start_link/0, start_anon/0]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

start_anon() -> gen_server:start_link(?MODULE, [], []).
start_link() -> gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

-record(state, {id, stash}).

init(_Args) -> { ok, #state{ id=hachi:generate_id(), stash=gb_trees:empty() } }.

retrieve(Stash, Id) -> gb_trees:lookup(Id, Stash).
store(Stash, Id, Val) -> gb_trees:enter(Id, Val, Stash).

handle_call({retrieve, <<Id:32/binary>>}, _From, State) -> {reply, retrieve(State#state.stash, Id), State};
handle_call(id, _From, State) -> {reply, State#state.id, State};
handle_call(stop, _From, State) -> {stop, normal, stopped, State};
handle_call(_Msg, _From, State) -> {reply, fail, State}.

handle_cast({store, <<Id:32/binary>>, Val}, State) -> {noreply, State#state{stash=store(State#state.stash, Id, Val)} };
handle_cast(_Msg, State) -> {noreply, State}.

handle_info(_Info, State) -> {noreply, State}.

terminate(_Reason, _State) -> ok.

code_change(_OldVsn, State, _Extra) -> {ok, State}.

